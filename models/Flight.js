const mongoose = require('mongoose');

const FlightSchema = new mongoose.Schema({
  user_id: {
    type: String,
    required: true
  },
  departure_icao: {
    type: String,
    required: true
  },
  arrival_icao: {
    type: String,
    required: true
  },
  flight_number: {
    type: String,
  },
  callsign: {
    type: String,
  },
  registration: {
    type: String,
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Flight = mongoose.model('flight', FlightSchema);
