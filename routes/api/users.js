const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');

const auth = require('../auth');

// Load User model
const User = require('../../models/User');

/**
 * @route GET api/users/test
 * @description tests users route
 * @access Public
 */
router.get('/test', (_, res) => res.send('user route testing!'));

/**
 * @route POST api/users/
 * @description Create a new user account
 * @access Public
 */
router.post('/', async (req, res) => {
  const { body: { user } } = req;

  if (!user.email) {
    return res.status(422).json({
      errors: {
        email: 'is required',
      },
    });
  }

  if (!user.password) {
    return res.status(422).json({
      errors: {
        password: 'is required',
      },
    });
  }

  const newUser = new User(user);

  newUser
    .setPassword(user.password)
    .then(async () => {

      return newUser
        .save()
        .then(() => res.json({ user: newUser.toAuthJSON() }));
    });
});

/**
 * @route POST api/users/checkauth
 * @description Check if a user has an existing JWT token
 * @access public
 */
router.post('/checkauth', (req, res, next) => {
  return passport.authenticate('jwt', { session: false }, (err, passportUser) => {
    if (err) {
      return next(err);
    }

    if (passportUser) {
      const user = passportUser;
      user.token = passportUser.generateJWT();

      return res.json({ user: user.toAuthJSON() });
    }

    return res.status(401).json({
      msg: 'Invalid auth',
    });
  })(req, res, next);
});

/**
 * @route POST api/users/login
 * @description Login to a user account
 * @access Public
 */
router.post('/login', (req, res, next) => {
  const { user } = req.body;

  if (!user.email) {
    return res.status(422).json({
      errors: {
        email: 'is required',
      },
    });
  }

  if (!user.password) {
    return res.status(422).json({
      errors: {
        password: 'is required',
      },
    });
  }

  return passport.authenticate('local', { session: false }, (err, passportUser) => {
    if (err) {
      return next(err);
    }

    if (passportUser) {
      const user = passportUser;
      user.token = passportUser.generateJWT();

      return res.json({ user: user.toAuthJSON() });
    }

    return res.status(401).json({
      msg: 'invalid email or password',
    });
  })(req, res, next);
});

router.get('/current', auth, async (req, res) => {
  const { user } = req;

  return User
    .findById(user)
    .then((user) => {
      if (!user) {
        return res.sendStatus(400);
      }

      return res.json({ user: user.toAuthJSON() });
    });
});

module.exports = router;
