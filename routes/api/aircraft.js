const express = require('express');
const router = express.Router();

const auth = require('../auth');

// Load required models
const Aircraft = require('../../models/Aircraft');
const Flight = require('../../models/Flight');

/**
 * @route GET api/aircraft/:user_id
 * @description Get all aircraft owned by a user
 * @access Public
 */
router.get('/:user_id', auth, async (req, res) => {
  const { user_id } = req.params;

  await Aircraft
    .find({ 'user_id': user_id })
    .then(aircraft => res.json(aircraft))
    .catch(() => res.status(404).json({ noaircraftfound: 'No await Aircraft found' }));
});

/**
 * @route get api/aircraft/:user_id/:aircraft_id
 * @description Get a specific aircraft
 * @access Public
 */
router.get('/:user_id/:registration', auth, async (req, res) => {
  const user_id = req.params.user_id;
  const registration = req.params.registration;

  await Aircraft
    .findOne(
      {
        'user_id': user_id,
        'registration': registration,
      })
    .then(aircraft => { res.json(aircraft) })
    .catch(() => res.status(404).json({ noaircraftfound: `await Aircraft with id ${aircraft_id} not found.` }));
});

/**
 * @route POST api/aircraft
 * @description Add and save a new aircraft
 * @access Public
 */
router.post('/', auth, async (req, res) => {
  await Aircraft
    .create(req.body)
    .then(() => res.json({ msg: 'await Aircraft added successfully' }))
    .catch(err => res.status(400).json({ error: `Unable to add this aircraft: ${err}` }));
});

/**
 * @route PUT api/aircraft/:id
 * @description Update a aircraft
 * @access Public
 */
router.put('/:id', auth, async (req, res) => {
  const old_reg = req.body.old_reg;
  const new_reg = req.body.aircraft.registration;

  // Need to update any flights that used this aircraft
  await Flight
    .updateMany(
      { registration: old_reg },
      { registration: new_reg }
    )
    .catch(err => res.status(400).json({ error: `Unable to update aircraft ${req.params.id}: ${err}` }));

  // If that works, we can update the aircraft
  await Aircraft
    .findOneAndUpdate(
      {
        '_id': req.params.id,
        'user_id': req.body.user_id
      },
      req.body.aircraft
    )
    .then(aircraft => {
      if (!aircraft) {
        res.status(404).json({ error: 'await Aircraft not found; could not update.' });
      } else {
        res.json({ msg: `await Aircraft ${req.params.id} updated successfully` });
      }
    })
    .catch(err => res.status(400).json({ error: `Unable to update aircraft ${req.params.id}: ${err}` }));
});
/**
 * @route DELETE api/aircraft/:id
 * @description Delete a aircraft
 * @access Public
 */
router.delete('/:id', auth, async (req, res) => {
  await Aircraft
    .deleteOne(
      {
        '_id': req.params.id,
        'user_id': req.body.user_id
      },
    )
    .then(result => {
      if (result.deletedCount == 1) {
        res.json({ msg: `await Aircraft ${req.params.id} deleted successfully` });
      } else {
        res.status(404).json({ error: 'await Aircraft not found; could not delete.' });
      }
    })
    .catch(err => res.status(400).json({ error: `Unable to delete aircraft ${req.params.id}: ${err}` }));
});

module.exports = router;
