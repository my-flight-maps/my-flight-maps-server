const mongoose = require('mongoose');
const passport = require('passport');
const LocalStrategy = require('passport-local');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const User = mongoose.model('user');

const tokenExtractor = (req) => {
  const { token } = req.body;
  return token;
};

let opts = {};
opts.jwtFromRequest = ExtractJwt.fromExtractors([tokenExtractor]);
opts.secretOrKey = process.env.JWT_SECRET;

passport.use(new LocalStrategy({
  usernameField: 'user[email]',
  passwordField: 'user[password]',
}, (email, password, done) => {
  User
    .findOne({ email })
    .then(async (user) => {
      const isValidPassword = await user.validatePassword(password);

      if (!user || !isValidPassword) {
        return done(null, false, {
          errors: { 'email or password': 'is invalid' }
        });
      }

      return done(null, user);
    }).catch(done);
}));

passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
  User
    .findOne({ _id: jwt_payload.id })
    .then(async (user) => {
      if (!user) {
        return done(null, false, {
          errors: { 'token': 'is invalid' }
        });
      }

      return done(null, user);
    }).catch(done);
}));


