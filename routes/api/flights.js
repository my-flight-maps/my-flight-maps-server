const express = require('express');
const router = express.Router();

const auth = require('../auth');

// Load models
const Flight = require('../../models/Flight');
const Aircraft = require('../../models/Aircraft');

/**
 * @route GET api/flights/test
 * @description tests flights route
 * @access Public
 */
router.get('/test', (_, res) => res.send('flight route testing!'));

/**
 * @route GET api/flights/:user_id
 * @description Get all flights by a user
 * @access Public
 */
router.get('/:user_id', auth, (req, res) => {
  const { user_id } = req.params;

  Flight
    .find({ 'user_id': user_id })
    .then(flights => res.json(flights))
    .catch(() => res.status(404).json({ noflightsfound: 'No Flights found' }));
});

/**
 * @route GET api/flights/:user_id/:flight_id
 * @description Get a specific flight
 * @access Public
 */
router.get('/:user_id/:flight_id', auth, (req, res) => {
  const { user_id, flight_id } = req.params;

  Flight
    .findOne(
      {
        '_id': flight_id,
        'user_id': user_id,
      })
    .then(flight => res.json(flight))
    .catch(() => res.status(404).json({ noflightsfound: `Flight with id ${flight_id} not found.` }));
});

/**
 * @route GET api/flights/:user_id/by_aircraft/:registration
 * @description Get all flights by a specific aircraft
 * @access Public
 */
router.get('/:user_id/by_aircraft/:registration', auth, (req, res) => {
  const { user_id, registration } = req.params;

  Flight
    .find(
      {
        'user_id': user_id,
        'registration': registration,
      })
    .then(flights => res.json(flights))
    .catch(() => res.status(404).json({ noflightsfound: `No flights found by aircraft ${registration}.` }));
});

/**
 * @route POST api/flights
 * @description Add and save a new flight
 * @access Public
 */
router.post('/', auth, (req, res) => {

  // We need to check if the registration exists first
  if (req.body.registration) {
    const aircraft = {
      "user_id": req.body.user_id,
      "registration": req.body.registration
    }

    Aircraft.
      findOneOrCreate(aircraft)
      .catch(err => res.status(400).json({ error: `Unable to add this flight: ${err}` }));
  }

  Flight
    .create(req.body)
    .then(() => res.json({ msg: 'Flight added successfully' }))
    .catch(err => res.status(400).json({ error: `Unable to add this flight: ${err}` }));
});

/**
 * @route PUT api/flights/:id
 * @description Update a flight
 * @access Public
 */
router.put('/:id', auth, (req, res) => {
  Flight
    .findOneAndUpdate(
      {
        '_id': req.params.id,
        'user_id': req.body.user_id
      },
      req.body.flight
    )
    .then(flight => {
      if (!flight) {
        res.status(404).json({ error: 'Flight not found; could not update.' });
      } else {
        res.json({ msg: `Flight ${req.params.id} updated successfully` });
      }
    })
    .catch(err => res.status(400).json({ error: `Unable to update flight ${req.params.id}: ${err}` }));
});

/**
 * @route DELETE api/flights/:id
 * @description Delete a flight
 * @access Public
 */
router.delete('/:id', auth, (req, res) => {
  Flight
    .deleteOne(
      {
        '_id': req.params.id,
        'user_id': req.body.user_id
      },
    )
    .then(result => {
      if (result.deletedCount == 1) {
        res.json({ msg: `Flight ${req.params.id} deleted successfully` });
      } else {
        res.status(404).json({ error: 'Flight not found; could not delete.' });
      }
    })
    .catch(err => res.status(400).json({ error: `Unable to delete flight ${req.params.id}: ${err}` }));
});

module.exports = router;
