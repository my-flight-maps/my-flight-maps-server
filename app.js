const express = require('express');
const connectDB = require('./config/db');
const cors = require('cors');
require('dotenv').config()

// routes
const flights = require('./routes/api/flights');
const users = require('./routes/api/users');
const aircraft = require('./routes/api/aircraft');

// config
const passport = require('passport');
require('./config/passport');

const app = express();

// Connect Database
connectDB();

// cors
app.use(cors({ credentials: true }));

// Init Middleware
app.use(express.json({ extended: false }));
app.use(passport.initialize());

// use Routes
app.use('/api/flights', flights);
app.use('/api/users', users);
app.use('/api/aircraft', aircraft);

const PORT = process.env.PORT || 8082;

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));

