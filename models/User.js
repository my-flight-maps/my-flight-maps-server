const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

// Used for salting passwords
const saltRounds = 10;

const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true
  },
  hash: {
    type: String,
    required: true
  }
});

UserSchema.methods.setPassword = async function (password) {
  await bcrypt
    .hash(password, saltRounds)
    .then(hash => {
      this.hash = hash
    })
    .catch(err => console.error(err.message));
};

UserSchema.methods.validatePassword = async function (password) {
  let result = false;
  await bcrypt
    .compare(password, this.hash)
    .then(res => {
      result = res;
    })
    .catch(err => console.error(err.message));

  return result;
};

UserSchema.methods.generateJWT = function () {
  const today = new Date();
  const expirationDate = new Date(today);
  expirationDate.setDate(today.getDate() + 30);

  return jwt.sign({
    email: this.email,
    id: this._id,
    exp: parseInt(expirationDate.getTime() / 1000, 10),
  }, process.env.JWT_SECRET);
};

UserSchema.methods.toAuthJSON = function () {
  return {
    _id: this._id,
    email: this.email,
    token: this.generateJWT(),
  };
};

module.exports = User = mongoose.model('user', UserSchema);
