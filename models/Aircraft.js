const mongoose = require('mongoose');

const AircraftSchema = new mongoose.Schema({
  user_id: {
    type: String,
    required: true
  },
  registration: {
    type: String,
    required: true
  },
  airline: {
    type: String,
  },
  icao_code: {
    type: String,
  }
});

// Check if an aircraft exists, otherwise create it
AircraftSchema.statics.findOneOrCreate =
  async function (doc) {
    const one = await this.findOne(doc);

    if (!one) {
      return await this.create(doc);
    } else {
      return one;
    }
  };

module.exports = Aircraft = mongoose.model('aircraft', AircraftSchema);
